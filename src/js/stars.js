const initStars = () => {

  console.log("Stars deployed");

  const d = document;
  //Select Canvas and context
  const canvas = d.querySelector('#canvas-1');
  const ctx = canvas.getContext('2d');

  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;


  // Colors for Stars
  let innerStarColor = [
    'rgba(255,255,255, 0.5)'
  ]

  let outerStarColor = [
     "rgba(238, 119, 82, 0.2)",
     "rgba(231, 60, 126, 0.2)",
     "rgba(35, 166, 213, 0.2)",
     "rgba(35, 213, 171, 0.2)",
  ]


  class Star {
    constructor(x, y, radius, moveX, moveY) {
      this.x = x;
      this.y = y;
      this.radius = radius;
      this.moveX = moveX;
      this.moveY = moveY;
      //Colors
      let innerColor = innerStarColor[Math.floor(Math.random() * innerStarColor.length)];
      let outerColor = outerStarColor[Math.floor(Math.random() * outerStarColor.length)];
      //Radi Glow
      let innerRadius = Math.floor(Math.random() + 0.8); //Inner Circle Star
      let outerRadius = innerRadius + 0.2; //Outer Circle Star
      let grd = ctx.createRadialGradient(x, y, innerRadius, x, y, outerRadius);
      //inner Color
      grd.addColorStop(0, innerColor);
      //outerColor
      grd.addColorStop(0.3, outerColor);
      //Faded
      grd.addColorStop(1, "rgba(255, 255, 255, 0.1)");
      //Draw
      this.draw = function() {
        ctx.beginPath();
        ctx.arc(x, y, radius, 0, Math.PI * 2);
        ctx.fillStyle = grd;
        ctx.fill();
      };
    }
  }


  //emptry Array for stars
  const stars = [];
  //Amount of Stars for Resolution
  let amountOfStars = window.innerWidth * window.innerHeight / 500;


  //create stars
  for (let i = 0; i < amountOfStars; i++) {
    let x = Math.random() * window.innerWidth;
    let y = Math.random() * window.innerHeight;
    let radius = (Math.random() + Math.random() * 0.2);

    stars.push(new Star(x, y, radius));
    stars[i].draw();
  }
console.log("End of Stars.js");
}


window.addEventListener('load', initStars);
