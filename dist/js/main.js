const initMain = () => {

  console.log("---->Main Script Initialized");
  const d = document;

  //Detect User Agent(Microsoft Internet Explorer and Redirect)
  checkBrowser = function() {
    //Detect Modern Browser
    // !! --> Converts to boolean Value
    // check(Regular Expression) The userAgent String for the String "Trident" oder "MSIE"
    // is IE --> Trident TRUE || MSIE TRUE
    let isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
    //When IE REDIRECT
    if (isIE) {
      window.location.href = "http://outdatedbrowser.com/de";
    } else {
      console.log("You are using a nice Browser");
    }
  }
  //CAll Functions
  checkBrowser();


  //Slideshow
  //set rotation Index
  let currentImg = 0;
  //call carousel function
  carousel();

  function carousel() {
    let slide = d.getElementsByClassName("slide");
    for (let i = 0; i < slide.length; i++) {
      slide[i].style.display = "none";
    }
    currentImg++;
    if (currentImg > slide.length) {
      currentImg = 1
    }
    slide[currentImg - 1].style.display = "block";
    slide[currentImg - 1].style.textAlign = 'center';
    setTimeout(carousel, 7000);
  }



  //Variables Mobile Navigation
  let menuButton = d.querySelector(".m-menu-button");
  let mobileNav = d.querySelector(".m-nav");
  let navItems = d.querySelectorAll(".nav-link");

  menuButton.addEventListener("click", function() {
    menuButton.classList.toggle("change");
    showNavigation();
  })

  showNavigation = function() {
    if (!(mobileNav.style.right === "0vw")) {
      mobileNav.style.right = "0vw";
    } else {
      mobileNav.style.right = "-100vw";
    }
  }

  closeNavigation = function() {
    for (let i = 0; i < navItems.length; i++) {
      navItems[i].addEventListener("click", function() {
        console.log(navItems[i] + " <-- clicked!!");
        mobileNav.style.right = "-100vw";
        menuButton.classList.toggle("change");
      })
    }
  }
  //Toggle functions
  closeNavigation();



  //  Smooth Scroll
  //href^= --> Matches any Element with the specified String
  d.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function(e) {
      //prevent jump
      e.preventDefault();
      //js native smooth scroll
      console.log(this.getAttribute('href'));
      d.querySelector(this.getAttribute('href')).scrollIntoView({
        behavior: 'smooth',
        block: 'start'
      });
    });
  });



  //DESKTOP NAVIGATION

  //ABOUT
  let toggleAbout = d.querySelector('.toggleAbout');
  let about = d.querySelector('#about');
  let aboutReturn = d.querySelector('.aboutReturn');

  toggleAbout.addEventListener('click', function() {
    console.log("Clicked, toggleAbout");
    about.style.top = '0';
  });

  aboutReturn.addEventListener('click', function() {
    about.style.top = '-100vh';
  });


  //CONTACT
  let toggleContact = d.querySelector('.toggleContact');
  let contact = d.querySelector('#contact');
  let contactReturn = d.querySelector('.contactReturn');

  toggleContact.addEventListener('click', function() {
    contact.style.left = '0';
  });

  contactReturn.addEventListener('click', function() {
    contact.style.left = "-100vw";
  });


  //Portfolio
  let togglePortfolio = d.querySelector('.togglePortfolio');
  let portfolio = d.querySelector('#portfolio');
  let portfolioReturn = d.querySelector('.portfolioReturn');

  togglePortfolio.addEventListener('click', function() {
    portfolio.style.left = '0';
  });

  portfolioReturn.addEventListener('click', function() {
    portfolio.style.left = '100vw';
  });
}

window.addEventListener('load', initMain);
