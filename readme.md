#Project Documentation and Description
Work in Development, especially the Desktop Version

###Description
My Personal Webpage, get in touch, look at my work and some basic infos about me.
Also my personal code-playground.

###To test locally
1. clone Repository
1. run `npm i`
1. start local development-server on port 8000
1. run `gulp browser-sync`






#Documentation
##Setup Gulp
* Install Gulp in Project Folder
* npm install --save-dev gulp
* create .gitignore, add node_modules

##Create gulpfile.js
1. gulpfile.js
  1. require gulp
  1. require modules
  1. setup tasks

###Reload on file change run: gulp browser-sync

###Install Browsersync
1. Check Global Packages install if it is not
  1. npm install browser-sync gulp --save-dev

###Install Autoprefixer
https://www.npmjs.com/package/gulp-autoprefixer
  1. $ npm install --save-dev gulp-autoprefixer

###Install Purge CSS
https://github.com/FullHuman/gulp-purgecss#readme
  1. npm install --save-dev gulp-purgecss
