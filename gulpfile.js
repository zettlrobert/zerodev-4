const gulp = require('gulp');
//Require browser-sync
const browserSync = require('browser-sync').create();
//Autoprefixer
const autoprefixer = require('gulp-autoprefixer');
//Purge CSS
const purgecss = require('gulp-purgecss');


const paths = {
  css: 'src/css/*.css',
  cleancss: 'src/css',
  js: 'src/js/*.js',
  src: 'src/*.html'
}

// async so function gets finished;
const test = async () => {
  console.log("Gulp test function running...");
}


function sync() {
  browserSync.init({
    proxy: "http://127.0.0.1:3000"
  });
  gulp.watch([paths.css, paths.src, paths.js]).on('change', browserSync.reload);
}


function cleancss () {
  return gulp.src(paths.css)
    //Purge unused css from
    .pipe(purgecss({
      content: [paths.src]
    }))
    //Prefix CSS
    .pipe(autoprefixer({
      browsers: ['last 5 versions'],
      cascade: false
    }))
    //Replace cleaned file
    .pipe(gulp.dest(paths.cleancss))
}


function dist () {
  return gulp.src([
              'src/*html',
              'src/css/*.css',
              'src/js/*.js',
              'src/media/**/**.*',
              'src/img/**.*'
            ],
            //Base for Copy, added to gulp.src
            {
              base: 'src'
            })
            .pipe(gulp.dest('dist'));
}


exports.test = test;
exports.cleancss = cleancss;
exports.sync = sync;
exports.dist = dist;
